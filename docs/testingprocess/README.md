# 📕Documentation: TestingProcess

A Testing Process

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **TestProcess** : Specific Performed Process for planning and executing the dynamic testing activities for the software in development.
* **LevelBasedTEsting** :  Levels in which testing activities are performed, such as Unit, Integration and System testing.
* **TestCoding** : Simple Performed Activity for implementing the Test Cases as Code artifacts (Test Code) to be used during Test Execution.
* **TestExecution** : Simple Performed Activity for effectively executing the Test Cases, by running the Test Code and producing the Test Results. 
* **TestCase** : Document containing the input data, expected results, steps and general conditions for testing some situation regarding a Code To Be Tested. 
* **TestCode** : Code produced for implementing a Test Case.
* **CodeTobeTested** : Portion of Code (software module) to be tested by a Test Case. 
* **TestResult** : Document containing the actual results and identified issues relative to a Test Case execution. 
