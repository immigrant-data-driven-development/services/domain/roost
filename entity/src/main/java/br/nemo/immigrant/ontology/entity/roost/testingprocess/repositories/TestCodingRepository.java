package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCoding;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestCodingRepository extends PagingAndSortingRepository<TestCoding, Long>, ListCrudRepository<TestCoding, Long> {

}
