package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCode;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestCodeRepository extends PagingAndSortingRepository<TestCode, Long>, ListCrudRepository<TestCode, Long> {

}
