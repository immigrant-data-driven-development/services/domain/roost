package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.LevelBasedTesting;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface LevelBasedTestingRepository extends PagingAndSortingRepository<LevelBasedTesting, Long>, ListCrudRepository<LevelBasedTesting, Long> {

}
