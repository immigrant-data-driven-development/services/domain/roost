package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestResult;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestResultRepository extends PagingAndSortingRepository<TestResult, Long>, ListCrudRepository<TestResult, Long> {

}
