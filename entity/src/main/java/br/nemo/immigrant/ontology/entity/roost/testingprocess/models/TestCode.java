package br.nemo.immigrant.ontology.entity.roost.testingprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.sysswo.system.models.Code;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "testcode")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class TestCode extends Code implements Serializable {

  @ManyToMany
  @JoinTable(
      name = "testcode_testcase",
      joinColumns = @JoinColumn(name = "testcode_id"),
      inverseJoinColumns = @JoinColumn(name = "testcase_id")
  )
  @Builder.Default
  private Set<TestCase> testcases = new HashSet<>();

   @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        TestCode elem = (TestCode) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "TestCode {" +
         "id="+this.id+


      '}';
  }
}
