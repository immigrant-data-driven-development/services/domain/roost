package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.CodeTobeTested;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface CodeTobeTestedRepository extends PagingAndSortingRepository<CodeTobeTested, Long>, ListCrudRepository<CodeTobeTested, Long> {

}
