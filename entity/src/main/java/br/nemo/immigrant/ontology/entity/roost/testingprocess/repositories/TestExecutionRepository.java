package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestExecution;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestExecutionRepository extends PagingAndSortingRepository<TestExecution, Long>, ListCrudRepository<TestExecution, Long> {

}
