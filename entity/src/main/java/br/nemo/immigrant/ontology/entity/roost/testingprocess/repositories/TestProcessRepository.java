package br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface TestProcessRepository extends PagingAndSortingRepository<TestProcess, Long>, ListCrudRepository<TestProcess, Long> {

}
