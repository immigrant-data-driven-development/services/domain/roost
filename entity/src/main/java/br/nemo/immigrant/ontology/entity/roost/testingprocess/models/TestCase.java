package br.nemo.immigrant.ontology.entity.roost.testingprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "testcase")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class TestCase extends Artifact implements Serializable {





  @ManyToMany(mappedBy = "testcases")
  @Builder.Default
  private Set<TestCode> testcodes = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "codetobetested_id")
  private CodeTobeTested codetobetested;

  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "testcase")
  @Builder.Default
  private TestResult testresult = null;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        TestCase elem = (TestCase) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "TestCase {" +
         "id="+this.id+


      '}';
  }
}
