package br.nemo.immigrant.ontology.entity.roost.testingprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "levelbasedtesting")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class LevelBasedTesting extends Activity implements Serializable {


/* 
 @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "levelbasedtesting")
  @Builder.Default
  Set<TestCoding> testcodings = new HashSet<>();

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "levelbasedtesting")
  @Builder.Default
  Set<TestExecution> testexecutions = new HashSet<>();
*/




  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        LevelBasedTesting elem = (LevelBasedTesting) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "LevelBasedTesting {" +
         "id="+this.id+


      '}';
  }
}
