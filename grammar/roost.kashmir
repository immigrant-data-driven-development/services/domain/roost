Configuration {
    software: "roost"
    about: "The Reference Ontology on Software Testing (ROoST) is partially integrated to SEON, representing the activities, artifacts and stakeholders involved in the Software Testing Process, considering only dynamic tests. Since the testing process is a technical process in the software development, ROoST shares concepts with other SEON networked ontologies."
    package_path: "br.nemo.immigrant.ontology"
    database_name: "roost"
    framework: "springdata"
}

from spo module process import SpecificProjectProcess, Activity
from spo module artifact import Artifact
from sysswo module system import Code

# "A Testing Process"
module TestingProcess {

     # "Specific Performed Process for planning and executing the dynamic testing activities for the software in development."
    entity TestProcess extends SpecificProjectProcess {
        levelBasedTEsting OneToMany LevelBasedTesting
    }
    
    # " Levels in which testing activities are performed, such as Unit, Integration and System testing."
    entity LevelBasedTesting extends Activity {

        testCoding OneToMany TestCoding
        testExecution OneToMany TestExecution
    }
    # "Simple Performed Activity for implementing the Test Cases as Code artifacts (Test Code) to be used during Test Execution."
    entity TestCoding extends Activity {
        using ManyToMany TestCase
        creates ManyToMany TestCode
    }

    # "Simple Performed Activity for effectively executing the Test Cases, by running the Test Code and producing the Test Results. "
    entity TestExecution extends Activity {
        executes ManyToMany TestCase
        using ManyToMany TestCode
    }
    # "Document containing the input data, expected results, steps and general conditions for testing some situation regarding a Code To Be Tested. "
    entity TestCase extends Artifact {
        
    }
    # "Code produced for implementing a Test Case."
    entity TestCode extends Code {
        implements ManyToMany TestCase
    }
    # "Portion of Code (software module) to be tested by a Test Case. "
    entity CodeTobeTested extends Code {
        tested OneToMany TestCase
    }
    # "Document containing the actual results and identified issues relative to a Test Case execution. "
    entity TestResult extends Artifact {
        testCase OneToOne TestCase
    }

}