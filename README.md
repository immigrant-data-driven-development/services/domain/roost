# roost
## 🚀 Goal
The Reference Ontology on Software Testing (ROoST) is partially integrated to SEON, representing the activities, artifacts and stakeholders involved in the Software Testing Process, considering only dynamic tests. Since the testing process is a technical process in the software development, ROoST shares concepts with other SEON networked ontologies.

## 📕 Domain Documentation

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest
3. Spring GraphQL


## 🔧 Install

1) Create a database with name roost with **CREATE DATABASE roost**.
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```
## 🔧 Usage

### Using Jar

```xml
<dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>roost</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```

### Using Webservice

Execute docker-compose:
```bash
docker-compose up
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-sro.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:8081](http://localhost:8081) to see Swagger
* Acess [http://localhost:8081/grapiql](http://localhost:8081/grapiql) to see Graphql.

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

