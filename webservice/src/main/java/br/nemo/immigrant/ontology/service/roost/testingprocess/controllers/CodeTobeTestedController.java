package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.CodeTobeTested;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.CodeTobeTestedRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.CodeTobeTestedInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CodeTobeTestedController  {

  @Autowired
  CodeTobeTestedRepository repository;

  @QueryMapping
  public List<CodeTobeTested> findAllCodeTobeTesteds() {
    return repository.findAll();
  }

  @QueryMapping
  public CodeTobeTested findByIDCodeTobeTested(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public CodeTobeTested createCodeTobeTested(@Argument CodeTobeTestedInput input) {
    CodeTobeTested instance = CodeTobeTested.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public CodeTobeTested updateCodeTobeTested(@Argument Long id, @Argument CodeTobeTestedInput input) {
    CodeTobeTested instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("CodeTobeTested not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteCodeTobeTested(@Argument Long id) {
    repository.deleteById(id);
  }

}
