package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCase;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCaseRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestCaseInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestCaseController  {

  @Autowired
  TestCaseRepository repository;

  @QueryMapping
  public List<TestCase> findAllTestCases() {
    return repository.findAll();
  }

  @QueryMapping
  public TestCase findByIDTestCase(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestCase createTestCase(@Argument TestCaseInput input) {
    TestCase instance = TestCase.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestCase updateTestCase(@Argument Long id, @Argument TestCaseInput input) {
    TestCase instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestCase not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestCase(@Argument Long id) {
    repository.deleteById(id);
  }

}
