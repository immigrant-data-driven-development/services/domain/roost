package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCoding;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCodingRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestCodingInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestCodingController  {

  @Autowired
  TestCodingRepository repository;

  @QueryMapping
  public List<TestCoding> findAllTestCodings() {
    return repository.findAll();
  }

  @QueryMapping
  public TestCoding findByIDTestCoding(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestCoding createTestCoding(@Argument TestCodingInput input) {
    TestCoding instance = TestCoding.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestCoding updateTestCoding(@Argument Long id, @Argument TestCodingInput input) {
    TestCoding instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestCoding not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestCoding(@Argument Long id) {
    repository.deleteById(id);
  }

}
