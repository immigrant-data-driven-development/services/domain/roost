package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestExecution;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestExecutionRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestExecutionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestExecutionController  {

  @Autowired
  TestExecutionRepository repository;

  @QueryMapping
  public List<TestExecution> findAllTestExecutions() {
    return repository.findAll();
  }

  @QueryMapping
  public TestExecution findByIDTestExecution(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestExecution createTestExecution(@Argument TestExecutionInput input) {
    TestExecution instance = TestExecution.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestExecution updateTestExecution(@Argument Long id, @Argument TestExecutionInput input) {
    TestExecution instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestExecution not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestExecution(@Argument Long id) {
    repository.deleteById(id);
  }

}
