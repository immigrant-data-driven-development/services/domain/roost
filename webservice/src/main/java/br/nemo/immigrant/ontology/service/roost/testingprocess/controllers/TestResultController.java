package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestResult;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestResultRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestResultInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestResultController  {

  @Autowired
  TestResultRepository repository;

  @QueryMapping
  public List<TestResult> findAllTestResults() {
    return repository.findAll();
  }

  @QueryMapping
  public TestResult findByIDTestResult(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestResult createTestResult(@Argument TestResultInput input) {
    TestResult instance = TestResult.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestResult updateTestResult(@Argument Long id, @Argument TestResultInput input) {
    TestResult instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestResult not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestResult(@Argument Long id) {
    repository.deleteById(id);
  }

}
