package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCode;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCodeRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testcode", path = "testcode")
public interface TestCodeRepositoryWeb extends TestCodeRepository {

}
