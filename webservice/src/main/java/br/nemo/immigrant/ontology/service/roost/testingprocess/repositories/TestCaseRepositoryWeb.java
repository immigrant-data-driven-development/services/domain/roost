package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCase;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCaseRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testcase", path = "testcase")
public interface TestCaseRepositoryWeb extends TestCaseRepository {

}
