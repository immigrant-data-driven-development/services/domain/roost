package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.LevelBasedTesting;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.LevelBasedTestingRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "LevelBasedTesting", path = "LevelBasedTesting")
public interface LevelBasedTestingRepositoryWeb extends LevelBasedTestingRepository {

}
