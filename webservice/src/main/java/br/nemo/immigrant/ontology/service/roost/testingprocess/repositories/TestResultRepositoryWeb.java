package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestResult;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestResultRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testresult", path = "testresult")
public interface TestResultRepositoryWeb extends TestResultRepository {

}
