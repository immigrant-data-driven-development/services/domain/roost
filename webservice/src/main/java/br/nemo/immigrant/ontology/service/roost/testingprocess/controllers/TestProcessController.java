package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestProcess;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestProcessRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestProcessController  {

  @Autowired
  TestProcessRepository repository;

  @QueryMapping
  public List<TestProcess> findAllTestProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public TestProcess findByIDTestProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestProcess createTestProcess(@Argument TestProcessInput input) {
    TestProcess instance = TestProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestProcess updateTestProcess(@Argument Long id, @Argument TestProcessInput input) {
    TestProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
