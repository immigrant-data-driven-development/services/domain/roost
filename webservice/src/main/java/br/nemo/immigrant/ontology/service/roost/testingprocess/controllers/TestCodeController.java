package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCode;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCodeRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.TestCodeInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestCodeController  {

  @Autowired
  TestCodeRepository repository;

  @QueryMapping
  public List<TestCode> findAllTestCodes() {
    return repository.findAll();
  }

  @QueryMapping
  public TestCode findByIDTestCode(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public TestCode createTestCode(@Argument TestCodeInput input) {
    TestCode instance = TestCode.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public TestCode updateTestCode(@Argument Long id, @Argument TestCodeInput input) {
    TestCode instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("TestCode not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteTestCode(@Argument Long id) {
    repository.deleteById(id);
  }

}
