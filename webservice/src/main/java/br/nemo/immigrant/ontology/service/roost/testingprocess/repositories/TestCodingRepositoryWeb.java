package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestCoding;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestCodingRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testcoding", path = "testcoding")
public interface TestCodingRepositoryWeb extends TestCodingRepository {

}
