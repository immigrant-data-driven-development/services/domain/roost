package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.CodeTobeTested;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.CodeTobeTestedRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "codetobetested", path = "codetobetested")
public interface CodeTobeTestedRepositoryWeb extends CodeTobeTestedRepository {

}
