package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestExecution;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestExecutionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testexecution", path = "testexecution")
public interface TestExecutionRepositoryWeb extends TestExecutionRepository {

}
