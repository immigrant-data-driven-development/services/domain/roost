package br.nemo.immigrant.ontology.service.roost.testingprocess.controllers;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.LevelBasedTesting;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.LevelBasedTestingRepository;
import br.nemo.immigrant.ontology.service.roost.testingprocess.records.LevelBasedTestingInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class LevelBasedTestingController  {

  @Autowired
  LevelBasedTestingRepository repository;

  @QueryMapping
  public List<LevelBasedTesting> findAllLevelBasedTestings() {
    return repository.findAll();
  }

  @QueryMapping
  public LevelBasedTesting findByIDLevelBasedTesting(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public LevelBasedTesting createLevelBasedTesting(@Argument LevelBasedTestingInput input) {
    LevelBasedTesting instance = LevelBasedTesting.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public LevelBasedTesting updateLevelBasedTesting(@Argument Long id, @Argument LevelBasedTestingInput input) {
    LevelBasedTesting instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("LevelBasedTesting not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteLevelBasedTesting(@Argument Long id) {
    repository.deleteById(id);
  }

}
