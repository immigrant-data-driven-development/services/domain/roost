package br.nemo.immigrant.ontology.service.roost.testingprocess.repositories;

import br.nemo.immigrant.ontology.entity.roost.testingprocess.models.TestProcess;
import br.nemo.immigrant.ontology.entity.roost.testingprocess.repositories.TestProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "testprocess", path = "testprocess")
public interface TestProcessRepositoryWeb extends TestProcessRepository {

}
